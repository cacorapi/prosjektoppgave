#!/usr/bin/env pybricks-micropython
from pybricks.hubs import EV3Brick
from pybricks.ev3devices import (Motor, TouchSensor, ColorSensor,
                                 InfraredSensor, UltrasonicSensor, GyroSensor)
from pybricks.parameters import Port, Stop, Direction, Button, Color
from pybricks.tools import wait, StopWatch, DataLog
from pybricks.robotics import DriveBase
from pybricks.media.ev3dev import SoundFile, ImageFile




#!/usr/bin/env pybricks-micropython
from pybricks.hubs import EV3Brick
from pybricks.ev3devices import (Motor, TouchSensor, ColorSensor,
                                 InfraredSensor, UltrasonicSensor, GyroSensor)
from pybricks.parameters import Port, Stop, Direction, Button, Color
from pybricks.tools import wait, StopWatch, DataLog
from pybricks.robotics import DriveBase
from pybricks.media.ev3dev import SoundFile, ImageFile








# Create your objects here.
ev3 = EV3Brick()




# Initialize the motors.
rotation = Motor(Port.C, Direction.COUNTERCLOCKWISE, [12, 36])
arm = Motor(Port.B, Direction.COUNTERCLOCKWISE, [8, 40])
claw = Motor(Port.A, Direction.COUNTERCLOCKWISE)




arm.control.limits(speed=60, acceleration=120)
claw.control.limits(speed=300, acceleration=2000)
rotation.control.limits(speed=60, acceleration=120)




# Initialize the color sensor.
ball_sensor = ColorSensor(Port.S2)



# Initialize the touch sensor
bowl = TouchSensor(Port.S1)
raised = TouchSensor(Port.S4)




#Code
ev3.speaker.beep()






#Sets arm in initial position, elevated
arm.run(15)
while not raised.pressed():
    wait(10)
arm.reset_angle(0)
arm.hold()


#Sets crane in initial position, over ramp
rotation.run(-60)
while not bowl.pressed():
    wait(10)
rotation.reset_angle(0)
rotation.hold()




#Sets claw in initial position. Claw closes and opens
#Minus makes the claw open
claw.run_target(200, -500)
ev3.speaker.beep()








#Function for picking up ball
def robot_pick(position):
    rotation.run_target(60, position)
    arm.run_target(60, -41)
    claw.run_target(200, 0)  
    arm.run_target(60, 0)
   


def robot_release(position):
    rotation.run_target(60, position)
    arm.run_target(60, -25)
    claw.run_target(200, -500)
    arm.run_target(60, 0)



#Where the different containers are placed
YELLOW = 200
GREEN = 130
BLUE = 80
BOWL = 0



while True:




    if ball_sensor.color() == Color.YELLOW:
        ev3.speaker.say("Yellow")
        robot_pick(BOWL)
        robot_release(YELLOW)


    elif ball_sensor.color() == Color.BLUE:
        ev3.speaker.say("Blue")
        robot_pick(BOWL)
        robot_release(BLUE)


    elif ball_sensor.color() == Color.BLACK:
        ev3.speaker.say("Green")
        robot_pick(BOWL)
        robot_release(GREEN)


    else:
        claw.run_target(200, 0)
        ev3.speaker.say("Job done!")
        fortsett = False
    wait(100)
